			# This code was produced by the CERI Compiler
	.data
FormatString1: .string "%llu\n"   # used by printf to display 64-bit unsigned integers
	.align 8
a:	.quad 0
	.text		# The following lines contain the program
	.globl main	# The main function must be visible from outside
main:			# The main function body :
	movq %rsp, %rbp	# Save the position of the stack's top
	push $5
	pop a
SQUARE :
	push a
	pop %rbx 
	push %rbx
	pop %rax
	mulq %rbx
	mulq %rbx
	push %rax
FIN :
	pop a
	push a
	pop %rdx                     	# The value to be displayed
	movq $FormatString1, %rsi    	# "%llu\n"
	movl    $1, %edi
	movl    $0, %eax
	call    __printf_chk@PLT
	movq %rbp, %rsp		# Restore the position of the stack's top
	ret			# Return from main function
