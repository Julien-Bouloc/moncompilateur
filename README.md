# CERIcompiler

A simple compiler.
From : Pascal-like imperative LL(k) langage
To : 64 bit 80x86 assembly langage (AT&T)

**Download the repository :**

> git clone git@framagit.org:jourlin/cericompiler.git

**Build the compiler and test it :**

> make test

**Have a look at the output :**

> gedit test.s

**Debug the executable :**

> ddd ./test

**Commit the new version :**

> git commit -a -m "What's new..."

**Send to your framagit :**

> git push -u origin master

**Get from your framagit :**

> git pull -u origin master

**This version Can handle :**

// Program := [DeclarationPart] StatementPart
// DeclarationPart := "[" Identifier {"," Identifier} "]"
// StatementPart := Statement {";" Statement} "."
// Statement := AssignementStatement
// AssignementStatement := Identifier ":=" Expression

// Expression := SimpleExpression [RelationalOperator SimpleExpression]
// SimpleExpression := Term {AdditiveOperator Term}
// Term := Factor {MultiplicativeOperator Factor}
// Factor := Number | Letter | "(" Expression ")"| "!" Factor
// Number := Digit{Digit}
// Identifier := Letter {(Letter|Digit)}

// AdditiveOperator := "+" | "-" | "||"
// MultiplicativeOperator := "*" | "/" | "%" | "&&"
// RelationalOperator := "==" | "!=" | "<" | ">" | "<=" | ">="  
// Digit := "0"|"1"|"2"|"3"|"4"|"5"|"6"|"7"|"8"|"9"
// Letter := "a"|...|"z"


//autre makefile
all:	test
clean:
		rm *.o *.s
		rm tokensiser.cpp
tokeniser.cpp:	tokeniser.l
		flex++ -d -o tokeniser.cpp tokeniser.l
tokeniser.o:	tokeniser.cpp
		g++ -c tokeniser.cpp
compilateur:	compilateur.cpp tokeniser.o
		g++ -ggdb -fno-pie -no-pie -o compilateur compilateur.cpp tokeniser.o
test:		compilateur test.p
		./compilateur <test.p 1>test.s
		gcc -ggdb -no-pie -fno-pie test.s -o test 

//tester le FOR
[a]

FOR a := 1 TO 10 DO DISPLAY a.

//tester le SWITCH
[a, b]
a := 1;
b := 2;
SWITCH a :
CASE 1 :
b := b+1
CASE 2 :
b := b+2
.

//tester le SQUARE

[a]
a := 5;
a := SQUARE( a );
DISPLAY a.

//tester le CUBE

[a]
a := 5;
a := CUBE( a );
DISPLAY a.

