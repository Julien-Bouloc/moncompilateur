//  A compiler from a very simple Pascal-like structured language LL(k)
//  to 64-bit 80x86 Assembly langage
//  Copyright (C) 2019 Pierre Jourlin
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//  
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//  
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.

// Build with "make compilateur"

 
#include <string>
#include <iostream>
#include <cstdlib>
#include <set>
#include <map>
#include <FlexLexer.h>
#include "tokeniser.h"
#include <cstring>

using namespace std;

enum OPREL {EQU, DIFF, INF, SUP, INFE, SUPE, WTFR};
enum OPADD {ADD, SUB, OR, WTFA};
enum OPMUL {MUL, DIV, MOD, AND ,WTFM};
enum TYPES {INT, BOOL, CHAR, DOUBLE, UNDEFINED};

TOKEN current;				// Current token


FlexLexer* lexer = new yyFlexLexer; // This is the flex tokeniser
// tokens can be read using lexer->yylex()
// lexer->yylex() returns the type of the lexicon entry (see enum TOKEN in tokeniser.h)
// and lexer->YYText() returns the lexicon entry as a string

	
set<string> DeclaredVariables;
map<string, enum TYPES> declaredVariables;
unsigned long TagNumber=0;

bool IsDeclared(const char *id){
	return DeclaredVariables.find(id)!=DeclaredVariables.end();
}

 
void Error(string s){
	cerr << "Ligne n°"<<lexer->lineno()<<", lu : '"<<lexer->YYText()<<"'("<<current<<"), mais ";
	cerr<< s << endl;
	exit(-1);
}

// Program := [DeclarationPart] StatementPart
// DeclarationPart := "[" Letter {"," Letter} "]"
// StatementPart := Statement {";" Statement} "."
// Statement := AssignementStatement
// AssignementStatement := Letter "=" Expression

// Expression := SimpleExpression [RelationalOperator SimpleExpression]
// SimpleExpression := Term {AdditiveOperator Term}
// Term := Factor {MultiplicativeOperator Factor}
// Factor := Number | Letter | "(" Expression ")"| "!" Factor
// Number := Digit{Digit}

// AdditiveOperator := "+" | "-" | "||"
// MultiplicativeOperator := "*" | "/" | "%" | "&&"
// RelationalOperator := "==" | "!=" | "<" | ">" | "<=" | ">="  
// Digit := "0"|"1"|"2"|"3"|"4"|"5"|"6"|"7"|"8"|"9"
// Letter := "a"|...|"z"
	
		
TYPES Identifier(void){
	enum TYPES type1;
	cout << "\tpush "<<lexer->YYText()<<endl;
	current=(TOKEN) lexer->yylex();
	type1=INT;
	return type1;
}

TYPES  Number(void){
	enum TYPES type1;
	cout <<"\tpush $"<<atoi(lexer->YYText())<<endl;
	current=(TOKEN) lexer->yylex();
	type1=INT;
	return type1;
}

TYPES Expression(void);			// Called by Term() and calls Term()

TYPES SquareStatement();
TYPES CubeStatement();

TYPES Factor(void){
	TYPES type1;
	
	if(current==RPARENT){
		
		current=(TOKEN) lexer->yylex();
		type1=Expression();
		//Expression();
		if(current!=LPARENT)
			Error("')' était attendu");		// ")" expected
		else
			current=(TOKEN) lexer->yylex();
	}
	else 
		if (current==NUMBER)
			type1=Number();
			//Number();
	     	else
				if(current==ID)
					type1=Identifier();
					//Identifier();
					else 
						if((current== KEYWORD && strcmp( lexer->YYText(),"SQUARE")==0))
						{
							type1=SquareStatement();
						}
						else 
							if((current== KEYWORD && strcmp( lexer->YYText(),"CUBE")==0))
							{
								type1=CubeStatement();
							}
				else
					Error("'(' ou chiffre ou lettre attendue");
					
	return type1;
					
}

// MultiplicativeOperator := "*" | "/" | "%" | "&&"
OPMUL MultiplicativeOperator(void){
	OPMUL opmul;
	if(strcmp(lexer->YYText(),"*")==0)
		opmul=MUL;
	else if(strcmp(lexer->YYText(),"/")==0)
		opmul=DIV;
	else if(strcmp(lexer->YYText(),"%")==0)
		opmul=MOD;
	else if(strcmp(lexer->YYText(),"&&")==0)
		opmul=AND;
	else opmul=WTFM;
	current=(TOKEN) lexer->yylex();
	return opmul;
}

// Term := Factor {MultiplicativeOperator Factor}
enum TYPES Term(void){
	
	enum TYPES type1;
	enum TYPES type2;
	
	OPMUL mulop;
	
	type1=Factor();
	
	while(current==MULOP){
		mulop=MultiplicativeOperator();		// Save operator in local variable
		
		type2=Factor();
		
		if(type2!=type1)
		{
			Error("types incompatibles");
		}
		
		cout << "\tpop %rbx"<<endl;	// get first operand
		cout << "\tpop %rax"<<endl;	// get second operand
		switch(mulop){
			case AND:
				
				cout << "\tmulq	%rbx"<<endl;	// a * b -> %rdx:%rax
				cout << "\tpush %rax\t# AND"<<endl;	// store result
				
				break;
			case MUL:
				
				cout << "\tmulq	%rbx"<<endl;	// a * b -> %rdx:%rax
				cout << "\tpush %rax\t# MUL"<<endl;	// store result
				
				break;
			case DIV:
			
				cout << "\tmovq $0, %rdx"<<endl; 	// Higher part of numerator  
				cout << "\tdiv %rbx"<<endl;			// quotient goes to %rax
				cout << "\tpush %rax\t# DIV"<<endl;		// store result
				
				break;
			case MOD:
			
				cout << "\tmovq $0, %rdx"<<endl; 	// Higher part of numerator  
				cout << "\tdiv %rbx"<<endl;			// remainder goes to %rdx
				cout << "\tpush %rdx\t# MOD"<<endl;		// store result
				
				break;
			default:
				Error("opérateur multiplicatif attendu");
		}
	}
	
	return type1;
}

// AdditiveOperator := "+" | "-" | "||"
OPADD AdditiveOperator(void){
	OPADD opadd;
	if(strcmp(lexer->YYText(),"+")==0)
		opadd=ADD;
	else if(strcmp(lexer->YYText(),"-")==0)
		opadd=SUB;
	else if(strcmp(lexer->YYText(),"||")==0)
		opadd=OR;
	else opadd=WTFA;
	current=(TOKEN) lexer->yylex();
	return opadd;
}

// SimpleExpression := Term {AdditiveOperator Term}
TYPES SimpleExpression(void){
	TYPES type1;
	TYPES type2;
	
	OPADD adop;
	
	type1=Term();
	
	while(current==ADDOP){
		
		adop=AdditiveOperator();		// Save operator in local variable
		
		type2=Term();
		
		if(type2!=type1)
		{
			Error("types incompatibles ");
		}
		
		cout << "\tpop %rbx"<<endl;	// get first operand
		cout << "\tpop %rax"<<endl;	// get second operand
		switch(adop){
			case OR:
				cout << "\taddq	%rbx, %rax\t# OR"<<endl;// operand1 OR operand2
				break;			
			case ADD:
				cout << "\taddq	%rbx, %rax\t# ADD"<<endl;	// add both operands
				
				break;			
			case SUB:	
				cout << "\tsubq	%rbx, %rax\t# SUB"<<endl;	// substract both operands
				break;
			default:
				Error("opérateur additif inconnu");
		}
		cout << "\tpush %rax"<<endl;			// store result
	}

	return type1;
}

// DeclarationPart := "[" Ident {"," Ident} "]"
void DeclarationPart(void){
	if(current!=RBRACKET)
		Error("caractère '[' attendu");
	cout << "\t.data"<<endl;
	cout <<"FormatString1: .string \"%llu\\n\"   # used by printf to display 64-bit unsigned integers"<<endl;
	cout << "\t.align 8"<<endl;
	
	current=(TOKEN) lexer->yylex();
	if(current!=ID)
		Error("Un identificater était attendu");
	cout << lexer->YYText() << ":\t.quad 0"<<endl;
	DeclaredVariables.insert(lexer->YYText());
	current=(TOKEN) lexer->yylex();
	while(current==COMMA){
		current=(TOKEN) lexer->yylex();
		if(current!=ID)
			Error("Un identificateur était attendu");
		cout << lexer->YYText() << ":\t.quad 0"<<endl;
		DeclaredVariables.insert(lexer->YYText());
		current=(TOKEN) lexer->yylex();
	}
	if(current!=LBRACKET)
		Error("caractère ']' attendu");
	current=(TOKEN) lexer->yylex();
}


enum TYPES Type(void){
	if(current!=KEYWORD){
		Error("type attendu");
	}
	if(strcmp(lexer->YYText(),"BOOL")==0){
		current=(TOKEN) lexer->yylex();
		return BOOL;
	}	
	else if(strcmp(lexer->YYText(),"INT")==0){
		current=(TOKEN) lexer->yylex();
		return INT;
	}
	else if(strcmp(lexer->YYText(),"DOUBLE")==0){
		current=(TOKEN) lexer->yylex();
		return DOUBLE;
	}
	else if(strcmp(lexer->YYText(),"CHAR")==0){
		current=(TOKEN) lexer->yylex();
		return CHAR;
	}
	else{
		Error("type inconnu");
	}	
	
	return UNDEFINED;
}
// Declaration := Ident {"," Ident} ":" Type
void VarDeclaration(void){
	set<string> idents;
	enum TYPES type;
	
	if(current!=ID)
		Error("identificater attendu");
	idents.insert(lexer->YYText());
	current=(TOKEN) lexer->yylex();
	
	while(current==COMMA){
		current=(TOKEN) lexer->yylex();
		if(current!=ID)
			Error("identificateur attendu");
		idents.insert(lexer->YYText());
		current=(TOKEN) lexer->yylex();
	}
	if(current!=COLON)
		Error("':' attendu");
	current=(TOKEN) lexer->yylex();
	type=Type();
	
	for (set<string>::iterator it=idents.begin(); it!=idents.end(); ++it){
	    switch(type){
			case BOOL:
			case INT:
				cout << *it << ":\t.quad 0"<<endl;
				break;
			case DOUBLE:
				cout << *it << ":\t.double 0.0"<<endl;
				break;
			case CHAR:
				cout << *it << ":\t.byte 0"<<endl;
				break;
			default:
				Error("type inconnu.");
		};
		declaredVariables[*it]=type;
	}
}

// VarDeclarationPart := "VAR" VarDeclaration {";" VarDeclaration} "."
void VarDeclarationPart(void){
	current=(TOKEN) lexer->yylex();
	VarDeclaration();
	while(current==SEMICOLON){
		current=(TOKEN) lexer->yylex();
		VarDeclaration();
	}
	if(current!=DOT)
		Error("'.' attendu");
	current=(TOKEN) lexer->yylex();
}



// RelationalOperator := "==" | "!=" | "<" | ">" | "<=" | ">="  
OPREL RelationalOperator(void){
	OPREL oprel;
	if(strcmp(lexer->YYText(),"==")==0)
		oprel=EQU;
	else if(strcmp(lexer->YYText(),"!=")==0)
		oprel=DIFF;
	else if(strcmp(lexer->YYText(),"<")==0)
		oprel=INF;
	else if(strcmp(lexer->YYText(),">")==0)
		oprel=SUP;
	else if(strcmp(lexer->YYText(),"<=")==0)
		oprel=INFE;
	else if(strcmp(lexer->YYText(),">=")==0)
		oprel=SUPE;
	else oprel=WTFR;
	current=(TOKEN) lexer->yylex();
	return oprel;
}






// Expression := SimpleExpression [RelationalOperator SimpleExpression]
 TYPES Expression(void){
	TYPES type1;
	TYPES type2;
	
	OPREL oprel;
	
	type1=SimpleExpression();
	
	if(current==RELOP){
		oprel=RelationalOperator();
		type2=SimpleExpression();
		
		if(type2!=type1)
		{
			Error("types incompatibles ");
		}
		
		cout << "\tpop %rax"<<endl;
		cout << "\tpop %rbx"<<endl;
		cout << "\tcmpq %rax, %rbx"<<endl;
		
		switch(oprel){
			case EQU:
				cout << "\tje Vrai"<<++TagNumber<<"\t# If equal"<<endl;
				break;
			case DIFF:
				cout << "\tjne Vrai"<<++TagNumber<<"\t# If different"<<endl;
				break;
			case SUPE:
				cout << "\tjae Vrai"<<++TagNumber<<"\t# If above or equal"<<endl;
				break;
			case INFE:
				cout << "\tjbe Vrai"<<++TagNumber<<"\t# If below or equal"<<endl;
				break;
			case INF:
				cout << "\tjb Vrai"<<++TagNumber<<"\t# If below"<<endl;
				break;
			case SUP:
				cout << "\tja Vrai"<<++TagNumber<<"\t# If above"<<endl;
				break;
			default:
				Error("Opérateur de comparaison inconnu");
		}
		cout << "\tpush $0\t\t# False"<<endl;
		cout << "\tjmp Suite"<<TagNumber<<endl;
		cout << "Vrai"<<TagNumber<<":\tpush $0xFFFFFFFFFFFFFFFF\t\t# True"<<endl;	
		cout << "Suite"<<TagNumber<<":"<<endl;
		return BOOL;
	}
	return type1;
}

// AssignementStatement := Identifier ":=" Expression
 string AssignementStatement(void){
	
	TYPES type1;
	TYPES type2;
	
	string variable;
	if(current!=ID)
		Error("Identificateur attendu ici");
	if(!IsDeclared(lexer->YYText())){
		cerr << "Erreur : Variable '"<<lexer->YYText()<<"' non déclarée"<<endl;
		exit(-1);
	}
	variable=lexer->YYText();
	type1=declaredVariables[variable];
	current=(TOKEN) lexer->yylex();
	if(current!=ASSIGN)
		Error("caractères ':=' attendus");
	current=(TOKEN) lexer->yylex();
	type2=Expression();
	if(type2!=type1)
	{
		Error("types incompatibles la");
	}
	cout << "\tpop "<<variable<<endl;
	
	return variable;
}



void Statement(void);

//IfStatement := "IF" Expression "THEN" Statement [ "ELSE" Statement ]
void IfStatement(){
	int tagNum= ++TagNumber;
	if(current== KEYWORD && strcmp( lexer->YYText(),"IF")==0)
    {
		
		cout<<"IF"<<tagNum<<":"<<endl;
        current=(TOKEN) lexer->yylex();//TOKEN suivant 
        
        TYPES type1 = Expression();    
        if(type1 != BOOL)
        
        {
				cout<<"error type boolean attendu"<<endl;
		}
		
        cout << "\tpop %rax"<<endl;//
        cout << "\tcmpq $0, %rax"<<endl;//compare le contenue du registre rax avec 0
        cout << "\tje ELSE"<<tagNum<<endl;//si égal renvoie à ELSE1
        
        
        if(current== KEYWORD && strcmp( lexer->YYText(),"THEN")==0) 
        {
			cout << "THEN"<<tagNum<<":"<<endl;
            current=(TOKEN) lexer->yylex();
            Statement();
       
            cout << "\tjmp FINsuite"<<tagNum<<endl;
            cout << "ELSE"<<tagNum<<":"<<endl;
        
            if(current== KEYWORD && strcmp( lexer->YYText(),"ELSE")==0)
            {            
                current=(TOKEN) lexer->yylex();
                Statement();
            }		       
        }
        else
        {
            Error("THEN attendu");
        }           
    }
    else
    {
        Error("IF attendu");
    }   
   cout<<"FINSuite:"<< endl;
 }

//WhileStatement := "WHILE" Expression DO Statement
void WhileStatement(){
	int tagNum= ++TagNumber;
	if(current== KEYWORD && strcmp( lexer->YYText(),"WHILE")==0)
    {
		
		cout<<"WHILE"<<tagNum<<":"<<endl;
        current=(TOKEN) lexer->yylex();//TOKEN suivant 
        
        TYPES type1 = Expression();    
        if(type1 != BOOL)
        
        {
				cout<<"error type boolean attendu"<<endl;
		}    
        
        cout << "\tpop %rax"<<endl;//
        cout << "\tcmpq $0, %rax "<<endl;//compare le contenue du registre rax avec 0
       // cout << "\tje DO"<<tagNum<<endl;//si égal renvoie à DO
        cout << "\tje Finsuite"<< endl ;
        
        if(current== KEYWORD && strcmp( lexer->YYText(),"DO")==0) 
        {
			
            current=(TOKEN) lexer->yylex();
            Statement();
       
            cout << "\tjmp WHILE"<<tagNum<<endl;
            
        }
        else
        {
            Error("DO attendu");
        }
          
    }
    else
    {
        Error("WHILE attendu");
    }
 
   cout<<"FINSuite:"<< endl;
 }

//ForStatement := "FOR" AssignementStatement "To" Expression "DO" Statement

void ForStatement(){
	int tagNum= ++TagNumber;
	if(current== KEYWORD && strcmp( lexer->YYText(),"FOR")==0)
    {
		
		cout<<"FOR"<<tagNum<<":"<<endl;
        current=(TOKEN) lexer->yylex();//TOKEN suivant 
        string var1 = AssignementStatement();    
        
       
        if(current== KEYWORD && strcmp( lexer->YYText(),"TO")==0) 
        {
			cout<<"TO"<<tagNum<<":"<<endl;
            current=(TOKEN) lexer->yylex();
            Expression();
       
			cout<<"\t push "<<var1<<endl;
        
			cout << "\tpop %rax"<<endl;
			cout << "\tpop %rbx"<<endl;
			
       
			cout << "\tcmpq %rbx, %rax "<<endl;//compare le contenue du registre rax avec rbx
			
			cout << "\tje FIN"<<tagNum<<endl;//si égal renvoie à FIN
       
           
            if(current== KEYWORD && strcmp( lexer->YYText(),"DO")==0)
            {
				cout << "DO"<<tagNum<<":"<<endl;
                current=(TOKEN) lexer->yylex();
                Statement();
                
                cout<<"\t push "<<var1<<endl;
                cout << "\tpop %rax"<<endl;
                cout << "\taddq $1, %rax "<<endl;
                cout<<"\tpush %rax"<<endl;
                cout<<"\tpop "<<var1<<endl;
                cout<<"\tjmp TO"<<tagNum<<endl;
                
            }
        
			else
			{
				Error("DO attendu");
			}
        }
        else
        {
            Error("To attendu");
        }
        
         cout<<"FIN"<< tagNum<<":"<<endl;   
    }
    else
    {
        Error("FOR attendu");
    }
    
    
   
 }
  

//Fonction ajoutée: SwitchCase
void SwitchStatement(void){ 
 
  int tagNum= ++TagNumber;
	if(current== KEYWORD && strcmp( lexer->YYText(),"SWITCH")==0)
    {
	current=(TOKEN) lexer->yylex();
	cout<<"SWITCH"<<tagNum<<" :"<<endl;
	TYPES type1=Expression();
	string var1;
	
	if(type1==BOOL){
		Error("BOOL pas accepter");
	}
	
	if (current!=COLON){
		Error("SWITCH':' attendu");
	}
	if (current==COLON){
		current=(TOKEN) lexer->yylex();
	}

	
	
	cout<<"\tpop %rcx"<<endl;
	
	int cmp = 0;
	while(strcmp(lexer->YYText(),"CASE")==0){
		current=(TOKEN) lexer->yylex();
		

		TYPES type2=Expression();
		
		if(type2==BOOL){
			Error("Error");
		}
		
		if (current!=COLON){
		Error("SWITCH':' attendu");
		}
		if (current==COLON){
		current=(TOKEN) lexer->yylex();
		}
		
		cout<<"\tmovq %rcx, %rax"<<endl;
		cout<<"\tpop %rbx"<<endl;
		cout<<"\tcmpq %rax,%rbx"<<endl;
		cout<<"\tjne CASE"<<tagNum<<'_'<<cmp<<endl;
		Statement(); 
		
		cout<<"\tjmp FIN"<<tagNum<<endl;
		cout<<"CASE"<<tagNum<<"_"<<cmp++<<" :"<<endl;
	}
	

	cout<<"FIN"<<tagNum<<" :"<<endl;
	

	}
}

//Fonction ajoutée Square
TYPES SquareStatement()
{
	if(current== KEYWORD && strcmp( lexer->YYText(),"SQUARE")==0)
	{
		
		cout<<"SQUARE"<<" :"<<endl;
		current=(TOKEN) lexer->yylex();	
		
		if (current!=RPARENT){
		Error("SQUARE'(' attendu");
		}
		
		if (current==RPARENT){
		current=(TOKEN) lexer->yylex();
		}
		TYPES type1=Expression();
		if(type1==BOOL){
		Error("BOOL pas accepter");
		}
		
		cout<<"\tpop %rbx "<<endl;
		cout<<"\tpush %rbx"<<endl;
		cout<<"\tpop %rax"<<endl;
		cout<<"\tmulq %rbx"<<endl;
		cout<<"\tpush %rax"<<endl;
		
		
		
		
	if (current!=LPARENT){
		Error("SQUARE')' attendu");
	}
	if (current==LPARENT){
		current=(TOKEN) lexer->yylex();
		}
		
		
		
	}
	
	cout<<"FIN"<<" :"<<endl;
	//TYPES type2;
	return INT;
}
	

//Fonction ajoutée Cube

TYPES CubeStatement()
{
	if(current== KEYWORD && strcmp( lexer->YYText(),"CUBE")==0)
	{
		
		cout<<"CUBE"<<" :"<<endl;
		current=(TOKEN) lexer->yylex();	
		
		if (current!=RPARENT){
		Error("CUBE'(' attendu");
		}
		
		if (current==RPARENT){
		current=(TOKEN) lexer->yylex();
		}
		TYPES type1=Expression();
		if(type1==BOOL){
		Error("BOOL pas accepter");
		}
		
		cout<<"\tpop %rbx "<<endl;
		cout<<"\tpush %rbx"<<endl;
		cout<<"\tpop %rax"<<endl;
		cout<<"\tmulq %rbx"<<endl;
		cout<<"\tmulq %rbx"<<endl;
		cout<<"\tpush %rax"<<endl;
		
		
		
		
	if (current!=LPARENT){
		Error("CUBE')' attendu");
	}
	if (current==LPARENT){
		current=(TOKEN) lexer->yylex();
		}
		
		
		
	}
	
	cout<<"FIN"<<" :"<<endl;
	//TYPES type2;
	return INT;
}
	




//BlockStatement := "BEGIN" Statement { ";" Statement } "END"
void BlockStatement(){
	int tagNum= ++TagNumber;
	if(current== KEYWORD && strcmp( lexer->YYText(),"BEGIN")==0)
    {

        current=(TOKEN) lexer->yylex();//TOKEN suivant 
        Statement();    
        
       
        
        
        if(current== KEYWORD && strcmp( lexer->YYText(),";")==0) 
        {
			
            current=(TOKEN) lexer->yylex();
            Statement();
       
          
            if(current== KEYWORD && strcmp( lexer->YYText(),"END")==0)
            {
                
                current=(TOKEN) lexer->yylex();
                
            }
        
			else
			{
				Error("END attendu");
			}
        }
        else
        {
            Error("; attendu");
        }
        
            
    }
    else
    {
        Error("BEGIN attendu");
    }
    
    
   //cout<<"FINSuite:"<< endl;
 }


void Display();

//Statement := AssignementStatement | IfStatement | WhileStatement | ForStatement | BlockStatement
void Statement(void){
	


	if(strcmp(lexer->YYText(),"IF")==0)
	{
		IfStatement();
	}
	
	else if(strcmp(lexer->YYText(),"WHILE")==0)
	{
		WhileStatement();
	}
	
	else if(strcmp(lexer->YYText(),"FOR")==0)
	{
		ForStatement();
	}
	
	else if(strcmp(lexer->YYText(),"BEGIN")==0)
	{
		BlockStatement();
	}
	
	else if(strcmp(lexer->YYText(),"DISPLAY")==0)
	{
		Display();
	}
	
	else if(strcmp(lexer->YYText(),"SWITCH")==0)
	{
		SwitchStatement();
	}
	else if(strcmp(lexer->YYText(),"SQUARE")==0)
	{
		SquareStatement();
	}
	else
	{
		AssignementStatement();
	}
	
}

// StatementPart := Statement {";" Statement} "."
void StatementPart(void){
	cout << "\t.text\t\t# The following lines contain the program"<<endl;
	cout << "\t.globl main\t# The main function must be visible from outside"<<endl;
	cout << "main:\t\t\t# The main function body :"<<endl;
	cout << "\tmovq %rsp, %rbp\t# Save the position of the stack's top"<<endl;
	Statement();
	while(current==SEMICOLON){
		current=(TOKEN) lexer->yylex();
		Statement();
	}
	if(current!=DOT)
		Error("caractère '.' attendu");
	current=(TOKEN) lexer->yylex();
}

// Program := [DeclarationPart] StatementPart
void Program(void){
	if(current==RBRACKET)
		DeclarationPart();
	StatementPart();	
} 

void Display(void){
	//cerr << "Fonction Display appelée" << endl;
	
		if(current== KEYWORD && strcmp( lexer->YYText(),"DISPLAY")==0)
	{
		//cerr << "Fonction Display appelée 2" << endl;
		current=(TOKEN) lexer->yylex();//TOKEN suivant 
		
		Expression();
	
		cout<<"\tpop %rdx                     \t# The value to be displayed"<<endl;
		cout<<"\tmovq $FormatString1, %rsi    \t# \"%llu\\n\""<<endl;
		cout<<"\tmovl    $1, %edi"<<endl;
		cout<<"\tmovl    $0, %eax"<<endl;
		cout<<"\tcall    __printf_chk@PLT"<<endl;
	
	}
	
}

int main(void){	// First version : Source code on standard input and assembly code on standard output
	// Header for gcc assembler / linker
	cout << "\t\t\t# This code was produced by the CERI Compiler"<<endl;
	// Let's proceed to the analysis and code production
	current=(TOKEN) lexer->yylex();
	Program();
	// Trailer for the gcc assembler / linker
	cout << "\tmovq %rbp, %rsp\t\t# Restore the position of the stack's top"<<endl;
	cout << "\tret\t\t\t# Return from main function"<<endl;
	if(current!=FEOF){
		cerr <<"Caractères en trop à la fin du programme : ["<<current<<"]";
		Error("."); // unexpected characters at the end of program
	}

}
		
			





